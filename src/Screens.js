import TuyAPI from 'tuyapi'

const screens = new TuyAPI({
    id: 'bffc24a8c4a5ce55f23xn8',
    key: '7eef42c4496693ad'});


// Find screens on network
screens.find().then(() => {
    // Connect to screens
    screens.connect().then();
});

// Add event listeners
screens.on('connected', () => {
    console.log('Connected to screens!');
});

screens.on('disconnected', () => {
    console.log('Disconnected from screens.');
});

screens.on('error', error => {
    console.log('Error!', error);
});



export default screens