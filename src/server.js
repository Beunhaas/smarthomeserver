console.log('server opstarten')

import express from 'express'
import bodyparser from "body-parser";
import light from './Lights.js'
import tv from './TV.js'
import screens from "./Screens.js";


const app = express();

app.use(bodyparser.json());



app.use(express.static('./static'));

app.get('/lights', ((req, res) => {
    light.toggle(1).then(r => res.json(r).end())
}))

app.get('/tv', ((req, res) => {
    tv.toggle(1).then(r => res.json(r).end())
}))

app.get('/screens', ((req, res) => {
    screens.toggle(1).then(r => res.json(r).end())
}))


const port = process.env.PORT || 8080;

const listener = app.listen(port,  () => {
    console.log(`Server started on port ${listener.address().port}.`)
})