import TuyAPI from 'tuyapi'

const light = new TuyAPI({
    id: '04800130c44f33a2b0f6',
    key: '14ea4515bf4a8f1f'});


let stateHasChanged = false;


// Find light on network
    light.find().then(() => {
        // Connect to light
        light.connect().then();
    });

// Add event listeners
    light.on('connected', () => {
        console.log('Connected to light!');
    });

    light.on('disconnected', () => {
        console.log('Disconnected from light.');
    });

    light.on('error', error => {
        console.log('Error!', error);
    });



export default light