import TuyAPI from 'tuyapi'

const tv = new TuyAPI({
    id: 'bf2fa8f87aa04e0019gjpi',
    key: 'ae6b891d3c4abe17'});


// Find tv on network
tv.find().then(() => {
    // Connect to tv
    tv.connect().then();
});

// Add event listeners
tv.on('connected', () => {
    console.log('Connected to tv!');
});

tv.on('disconnected', () => {
    console.log('Disconnected from tv.');
});

tv.on('error', error => {
    console.log('Error!', error);
});



export default tv